# Backend A2O Web App Developer

## Docker build

Run `docker-compose up -d` for build project in Docker. The url the backend is `http://localhost:3000/`.

## Development server requirements
- Node v14.21.2
- npm 6.14.0
- typescript 4.9.5

## Development server
Run `npm install` in root project for install dependeces.

Run `npm start` for a dev server. The url the backend is `http://localhost:3000/`.


## Endpoints

- ## Problem 1

### `POST /problem/1`

- `n` (number)
- `k` (number)
- `rq` (number)
- `cq` (number)
- `obstacles` (string[]) Array of strings separated by a space represent the obstacles.


#### Example request:

```json
POST /problem/1
Content-Type: application/json

{
  "n": 5,
  "k": 3,
  "rq": 4,
  "cq": 3,
  "obstacles": ["5 5","4 2","2 3"]
}
```

#### Example response:
```json
Status: 200 OK
Content-Type: application/json

{
  "result": 10
}
```

- ## Problem 2

### `POST /problem/2`

- `t` (string)

#### Example request:
```json
POST /problem/2
Content-Type: application/json

{
  "t":"aaaaaa"
}
```
#### Example response:
```json
Status: 200 OK
Content-Type: application/json

{
  "maxValue": 12
}
```