FROM node:14.21.2-alpine

WORKDIR /app

RUN apk add --no-cache \
    musl-dev \
    libc-dev

COPY package.json package-lock.json tsconfig.json ./
COPY src ./src

RUN npm install

RUN npm run build

EXPOSE 3000

CMD ["node", "dist/index.js"]
