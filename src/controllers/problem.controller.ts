import { inject } from "inversify";
import { BaseHttpController, controller,httpPost, request, requestBody, response, results } from "inversify-express-utils";
import TYPES from "../types";
import { Problem1, Problem2 } from "../services";
import { Request, Response } from "express";

@controller('/problem')
export class Controller extends BaseHttpController {
  constructor(@inject(TYPES["problem-1"]) private _problem1:Problem1,
  @inject(TYPES["problem-2"]) private _problem2:Problem2) {
    super();
  }

  @httpPost('/1')
  async resolveProblem1(@request() req: Request, @response() res:Response) {
    try {
      const {body} = req
      const obstacles = body.obstacles.map((obstacle:string) => obstacle.split(' ').map(obs => +obs))
      const result = await this._problem1.queensAttack(body.n,body.k,body.rq,body.cq, obstacles);
      res.send({result})
      return results
    } catch (error: any) {
      return this.badRequest(error);
    }
  }

  @httpPost('/2')
  async resolveProblem2(@requestBody() body: any, @response() res: Response) {
    try {
     return res.send({maxValue:await this._problem2.maxValue(body.t)});
    } catch (error: any) {
     return this.badRequest(error.toString());
    }
  }
}