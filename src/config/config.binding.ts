import { Container } from "inversify";
import TYPES from "../types";
import { Problem1, Problem2 } from "../services";

const container = new Container();

container.bind<Problem1>(TYPES["problem-1"]).to(Problem1)
container.bind<Problem2>(TYPES["problem-2"]).to(Problem2)

export default container;