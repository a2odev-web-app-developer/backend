import express, { Application } from 'express';
import cors from 'cors';
import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import container from './config/config.binding';


import './controllers';
class Server {
  private app: Application;
  private port: number;

  constructor() {
    this.app = express();
    this.port = +(process.env.PORT ?? 3000);

    this.middlewares();
  }

  middlewares() {
    // CORS
    // CORS
    this.app.use(cors());

    // Lectura de body
    this.app.use(express.json({ limit: '50mb' }));
    this.app.use(express.urlencoded({ limit: '50mb', extended: true }));

    // Carpeta publica
    this.app.use(express.static('../public'));
  }

  listen() {
    const server = new InversifyExpressServer(container, null, null, this.app);

    const serverInstance = server.build();
    serverInstance.listen(this.port);

    console.log('Servidor corriendo en puerto ' + this.port);
  }
}

export default Server;
