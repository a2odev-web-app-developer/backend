
const TYPES = {
  'problem-1': Symbol.for('Problem-1'),
  'problem-2': Symbol.for('Problem-2')
}
export default TYPES;
