import { injectable } from "inversify";

@injectable()
export class Problem2 {
  calculateF(s: string, t: string): number {
    let Fs = 0;
    const substringLength = s.length
    while(t.length > 0 ){
      if(t.substring(0,substringLength) === s) {
        Fs += 1;
      }
      t = t.substring(1)
    }
    return Fs * substringLength
  }
  
  maxValue(t: string): Promise<number> {
    return new Promise<number>((resolve, reject)=> {

      try {
        let maxValue = 0;
        for (let i = 0; i < t.length; i++) {
          for (let j = i + 1; j <= t.length; j++) {
            const substring = t.substring(i, j);
            const fValue = this.calculateF(substring, t);
            maxValue = Math.max(maxValue, fValue);
          }
        }
        resolve(maxValue);
      } catch (error) {
        reject(error)
      }

    })
  }    
}