import { injectable } from "inversify";

@injectable()
export class Problem1 {
  
  queensAttack(n:number,k:number,rq:number,cq:number,obstacles:number[][]):Promise<number> {
    return new Promise((resolve,reject)=> {
      try {
        if(!this.validateBoard(n) || !this.validateAmountObstacles(k,obstacles.length))
          return reject({error:'error data invalid'})
        const queenMoves = [[0,1],[0,-1],[1,0],[-1,0],[1,1],[-1,1],[1,-1],[-1,-1]]
        let squaresAttacked = 0;
        const obstaclesSet = new Set(obstacles.map(([r, c]) => `${r},${c}`));
        for(const [moveRow, moveCol] of queenMoves) {
          let col = cq , row=rq;
          
          while (1 <= row + moveRow && row + moveRow <= n && 1 <= col + moveCol && col + moveCol <= n) {
            row += moveRow;
            col += moveCol;
      
            if (obstaclesSet.has(`${row},${col}`)) {
              break;
            }
      
            squaresAttacked++;
          }
        }
        resolve(squaresAttacked)
      } catch (error) {
        reject(error)
      }
    })
  }

  validateBoard(n:number) {
    return n > 0 && n <= Math.pow(10,5) ? true : false
  }

  validateAmountObstacles(k:number, obstaclesLength:number) {
    return (k >= 0 && k <= Math.pow(10,5)) && obstaclesLength === k ? true : false
  }

}